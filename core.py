# MIT License
#
# Dialogical - Simple Mixer chat client.
# Copyright (c) 2019 Anthony Goins
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import getopt

def main(argv):
    usage = (
                "Usage: "+ argv[0] +" [-h] [-g=<gui>] -v\n"
                "  -h    --help               Display this usage information\n"
                "  -g    --gui                Select the Graphical User Interface\n"
                "  -v    --version            Display the version number\n"
            )

    # We'll be adding more flags eventually...  Bare bones right now...
    long_opts = [ "help", "gui", "version" ]
    opts, args = getopt.gnu_getopt(sys.argv[1:], "hgv", long_opts)

    output = None
    quiet = False
    for o, a in opts:
        if o in ("-h", "--help"):
            print(usage)
            sys.exit()
        elif o in ("-g", "--gui"):
            output = a
        elif o in ("-v", "--version"):
            output = a
        else:
            assert False, "unhandled options"
    print(output)

if __name__ == "__main__":
    main(sys.argv)

