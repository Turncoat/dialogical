
from setuptools import setup, find_packages

setup(
    name="Dialogical",
    version="0.0.1",
    url="https://gitlab.com/Turncoat/dialogical",
    author="Anthony Goins",
    author_email="the.green.machine@gmail.com",
    description="Simple chat client for mixer",
    license="MIT",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[]
)